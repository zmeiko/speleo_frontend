import {connect} from 'react-redux'
import ArticleView from '../components/ArticleView'
import {callGetArticleById} from '../actions/ArticleAction'

const mapStateToProps = (state, props) => {
    let article = state.articles[props.match.params.id]
    return {
        article:article 
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    const id = ownProps.match.params.id;
    return {
        loadArticle: () => {
            dispatch(callGetArticleById(id))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ArticleView)