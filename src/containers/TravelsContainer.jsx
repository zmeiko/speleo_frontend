import { connect } from 'react-redux'
import ArticlesList from '../components/ArticlesList'
import { callGetAllArticlesByTag } from '../actions/ArticleAction'
import { getAllTravelsArticles } from '../selectors/ArticleSelector.js'

const mapStateToProps = (state, props) => {
    return {
        title: "Информация о походах",
        articles: getAllTravelsArticles(state),
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatchCallGetAllArticles: () => {
            dispatch(callGetAllArticlesByTag('travel'))
        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(ArticlesList)