import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class Navigation extends Component {
  renderSuggestMenuItem = () => {
    const { isLogin } = this.props
    if (isLogin) {
      return <li><NavLink to="/suggest" className="menuitem">Предложить новость</NavLink></li>
    } else {
      return <div />
    }
  }

  render() {
    return (
      <nav className="navbar navbar-default navbar-fixed-top">
        <div className="container">
          <div className="navbar-header">
            <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span className="sr-only">Toggle navigation</span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
            </button>
          </div>
          <div id="navbar" className="navbar-collapse collapse">
            <ul className="nav navbar-nav">
              <li><NavLink to="/news" className='menuitem' activeClassName="active">Главная</NavLink></li>
              <li><NavLink to="/travels" className='menuitem' activeClassName="active">Походы</NavLink></li>
              <li><NavLink to="/reports" className='menuitem' activeClassName="active">Отчеты</NavLink></li>
              <li><NavLink to="/about" className='menuitem' activeClassName="active">О Клубе</NavLink></li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}

export default Navigation;
