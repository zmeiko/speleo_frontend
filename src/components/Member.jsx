import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {Grid, Image} from 'react-bootstrap';
import {RichText} from 'prismic-reactjs';


class SingleMember extends Component {
    render() {
        const {name, image} = this.props;
        return <Grid fluid>

            <Image responsive src={'url' in image ? image.url : '/media/icon-profile.png'} circle/>

            <p className="member-name">
                {RichText.asText(name)}
            </p>
        </Grid>
    }
}

SingleMember.propTypes = {
    name: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
}

SingleMember.defaultProps = {}

export default SingleMember
