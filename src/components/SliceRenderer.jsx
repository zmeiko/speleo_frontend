import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {RichText} from 'prismic-reactjs';
import {Grid, Row, Carousel} from 'react-bootstrap';
import {linkResolver, htmlSerializer} from '../utils/htmlserializer';
import MembersList from './MembersList';
import ReportList from "./ReportList";


class SliceRenderer extends Component {
    render() {
        const pageContent = this.props.slice.body.map((slice, index) => {
            if (slice.slice_type === 'text') {
                return (
                    <Row className="article-text" key={index}>
                        {RichText.render(slice.primary.text, linkResolver, htmlSerializer)}
                    </Row>
                );
            } else if (slice.slice_type === 'image_gallery') {
                return (
                    <Row className="image-gallery" key={index}>
                        <Carousel prevIcon={""} nextIcon={""}>
                            {slice.items.map(item => {
                                return <Carousel.Item>
                                    <img alt="" src={item.gallery_image.url}/>
                                </Carousel.Item>
                            })}
                        </Carousel>
                    </Row>
                )
            } else if (slice.slice_type === 'members') {
                return <Row key={index}>
                    <MembersList members={slice.items}/>
                </Row>
            } else if (slice.slice_type === 'report') {
                return <Row key={index}>
                    <ReportList reports={slice.items}/>
                </Row>
            } else {
                return <div/>
            }
        });
        return (
            <div>
                {pageContent}
            </div>
        );
    }
}

SliceRenderer.propTypes = {
    slice: PropTypes.object
};

export default SliceRenderer;