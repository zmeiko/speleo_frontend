import React, { Component } from 'react';
import PropTypes from 'prop-types'
import Report from "./Report";

class ReportList extends Component {
    render() {
        const { reports } = this.props;
        return (
            <div className="report-list">
                {reports.map((m, index) => <Report key={index} date={m.date} title={m.report_name} url={m.file.url} />)}
            </div>)

    }
}

ReportList.propTypes = {
    reports: PropTypes.array.isRequired,
};

ReportList.defaultProps = {

};

export default ReportList
