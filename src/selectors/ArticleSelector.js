import {
  createSelector
} from 'reselect'

const getArticles = (state, props) => state.articles
const getTravelNotesTag = (state, _) => "notes"
const getAboutTag = (state, _) => "about_us"
const getNewsTag = (state, _) => "news"
const getTravelTag = (state, _) => "travel"
const getReportTag = (state, _) => "reports"

const filterByTag = (articles, tag) => {
  if (tag !== undefined) {
    return Object.values(articles).filter(item => item.tags.find(_tag => _tag === tag) !== undefined)
  }
  return []
}

export const getAllNews = createSelector(
  [getArticles, getNewsTag], filterByTag
)

export const getAllTravelsArticles = createSelector(
  [getArticles, getTravelTag], filterByTag
)

export const filterArticlesByTag = createSelector([getArticles, getAboutTag], filterByTag)

export const getReportArticle = createSelector([getArticles, getReportTag], filterByTag)

export const getLastNoteArticles = createSelector(
  [getAllNews, getTravelNotesTag],
  filterByTag
)