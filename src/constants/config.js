module.exports = function(){
    switch(process.env.NODE_ENV){
        case 'development':
            return {"host":"http://localhost:3001"};

        case 'production':
            return {"host":"https://speleo-app.herokuapp.com"};

        default:
            throw new Error("NODE_ENV not define");
    }
};