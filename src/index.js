import React from 'react';
import store from './store/configureStore';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import ReactDOM from 'react-dom';
import ScrollToTop from './containers/ScrollToTop'
import App from './containers/App';
import './styles/main.css'

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <ScrollToTop>
        <App />
      </ScrollToTop>
    </BrowserRouter>
  </Provider>,
  document.getElementById('app')
);
