import * as types from '../constants/ActionTypes'
const articlesInitialState = {}
const articles = (state = articlesInitialState, action) => {
    switch (action.type) {
        case types.ARTICLE_ADD_ALL:
            return Object.assign({}, state, action.data.reduce((prev, curr) => {
                prev[curr.id] = curr;
                return prev;
            }, {}));
        case types.ARTICLE_GET_ONE:
        case types.ARTICLE_UPDATE:
            return Object.assign({}, state, {[action.data.id]: action.data});
        default:
            return state
    }
}
export default articles;