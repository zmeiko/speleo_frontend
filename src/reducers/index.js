import {
    combineReducers
} from 'redux';
import articles from './ArticleReducer'


const mainReducer = combineReducers({
    articles
});

export default mainReducer;